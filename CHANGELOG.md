# gemnasium-db-toolbox changelog

## v1.3.1 - 2020-06-30
- Compile with Go 1.14.2 (!18)

## v1.3.0 - 2019-06-26
- Add rubygem support to `filter` command (!7)

## v1.2.0 - 2019-06-25
- Add `filter` command to filter affected versions according to affected range (npm only) (!6)

## v1.1.0 - 2019-06-24
- Post list of affected versions to the API when available (!4)
- Add `prefill` command to pre-fill the affected versions with all available versions (!4)

## v1.0.0 - 2019-06-07
- Initial release
