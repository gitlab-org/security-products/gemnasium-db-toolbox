FROM node:11-alpine

RUN apk add -U --no-cache ca-certificates ruby
RUN npm install -g semver
COPY univers/gem/gemver.rb /usr/local/bin/gemver

ADD gemnasium-db-toolbox /
ENTRYPOINT ["/gemnasium-db-toolbox"]
WORKDIR /gemnasium-db
