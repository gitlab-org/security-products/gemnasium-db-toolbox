# DEPRECATED

This project has been archived and is no longer used. 

The  [Gemnasium vulnerability database](https://gitlab.com/gitlab-org/security-products/gemnasium-db) now
benefits from automation with https://gitlab.com/gitlab-org/security-products/advisory-db-curation-tools.

<details>
<summary>see README</summary>

# gemnasium-db-toolbox

Tools to update the Gemnasium vulnerability database

## Usage & workflow

gemnasium-db-toolbox can check, inspect and publish
a YAML file containing a security advisory.

Usually the first step is to make validate the security advisory using the `check` sub-command.
Note that the `check` sub-command connects to the Gemnasium API.
This is necessary in order to ensure that, for instance,
the fixed versions listed in the YAML document are found by the server.

In the following example, `check` reports two errors affecting the fields
`fixed_versions` and `affected_range` of the YAML:

```
$ ./gemnasium-db-toolbox check pypi/urllib3/CVE-2019-11324.yml 
fixed_versions, 1.24.5: version cannot be found
affected_range, <~1.24.1: version range cannot be parsed
```

Next step is to preview and check the affected versions, fixed versions and ignored versions
for the security advisory about to be published. This is done using the `preview` sub-command.

```
$ ./gemnasium-db-toolbox preview pypi/urllib3/CVE-2019-11324.yml
fixed: 1.24.2
affected: 1.24.1 1.24 1.23 1.22 1.21.1 1.21 1.20 1.19.1 1.19 1.18.1 1.18 1.17 1.16 1.15.1 1.15 1.14 1.13.1 1.13 1.12 1.11 1.10.4 1.10.3 1.10.2 1.10.1 1.10 1.9.1 1.9 1.8.3 1.8.2 1.8 1.7.1 1.7 1.6 1.5 1.4 1.3 1.2.2 1.2.1 1.2 1.1 1.0.2 1.0.1 1.0 0.2 0.3 0.3.1 0.4.0 0.4.1
ignored: 1.25.3 1.24.3 1.25.2 1.25.1 1.25
```

Once the version lists are reviewed, the advisory can be published using the `publish` sub-command:

```
$ ./gemnasium-db-toolbox publish pypi/urllib3/CVE-2019-11324.yml
```

### Setting the affected versions locally

The version syntax used by the package manager may not be supported by the Gemnasium API,
resulting in an error when processing the advisory:

```
$ ./gemnasium-db-toolbox preview gem/rack-protection/CVE-2018-1000119.yml
gem: version syntax not supported
```

To work around this limitation, you have to set the `affected_versions_array` field
with a list of all the affected versions.

Because setting the affected versions manually is both tedious and error prone,
the list can be pre-filled with all the versions of the affected package
using the `prefill` command.

```
$ ./gemnasium-db-toolbox prefill gem/rack-protection/CVE-2018-1000119.yml

$ grep -A 5 affected_versions_array gem/rack-protection/CVE-2018-1000119.yml
affected_versions_array:
- 0.1.0
- 1.0.0
- 1.1.2
- 1.1.3
- 1.1.4
```

Once pre-filled, the affected versions can be edited manually before publishing the advisory.
The value of `affected_range` will be stored in the database
but it won't be processed by the Gemnasium API.

In the case of npm and Ruby gems, the affected versions can be filtered using the `filter` command.
This command uses [node-semver](https://github.com/npm/node-semver#usage) to apply the affected range
and filter out versions that don't match.

```
$ ./gemnasium-db-toolbox filter gem/rack-protection/CVE-2018-1000119.yml
```

## Authentication

gemnasium-db-toolbox needs a
[JSON Web Token](https://en.wikipedia.org/wiki/JSON_Web_Token)
to connect to the Gemnasium API.

Here's how to retrieve the JWT using the web interface:

- sign in https://deps.sec.gitlab.com/ using a web browser
- copy the value of `gmsJwtToken` key from the Local Storage
  - by opening the javascript console and typing `localStorage.getItem('gmsJwtToken')`
  - or by opening the developer tools -> Application -> Storage (on Chrome)

NOTE: If the API returns `Token is invalid or expired` then sign out and sign in again.

The JWT is passed on to gemnasium-db-toolbox
via the environment variable `GEMNASIUM_JWT` or the `--jwt` option.

To avoid leaking the token on your marchine
it's best to set the JWT using the environment variable and `pbpaste` (on Mac OSX) or equivalent:

```
export GEMNASIUM_JWT=$(pbpaste)
```

## Using the Docker image

gemnasium-db-toolbox is available as a Docker image. Here's how to use it to publish a security advisory:

1. `git clone git@gitlab.com:gitlab-org/security-products/gemnasium-db.git`
1. `cd gemnasium-db`
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/gemnasium-db \
      --env GEMNASIUM_JWT \
      registry.gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox:${VERSION:-latest} \
      check pypi/urllib3/CVE-2019-11324.yml
    ```

`VERSION` can be replaced any version listed in the [changelog](CHANGELOG.md).

`pypi/urllib3/CVE-2019-11324.yml` is path of the security advisory you'd like to publish.

## Development

Go 1.13 or higher is highly recommended to build gemnasium-db-toolbox.

```sh
go build
```

Use `go test` to run the test suite:

```sh
go test -v ./...
```

## Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).

</details>
