package client

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

// PostMode is an int representing either Preview or Publish
type PostMode int

const (
	// PostModePreview int for preview
	PostModePreview PostMode = iota
	// PostModePublish int for publish 
	PostModePublish
)

// Advisory contains a model Advisory and a list of versions
type Advisory struct {
	model.Advisory
	Versions []model.Version `json:"versions"`
}

// PostAdvisory sends POST advisory request 
func (c Client) PostAdvisory(adv *model.Advisory, mode PostMode) (*Advisory, error) {
	// url
	var u = c.baseURL + "/api/advisories"
	if mode != PostModePublish {
		u = u + "?preview=1"
	}

	// prepare request
	payload, err := json.Marshal(adv)
	buf := bytes.NewBuffer(payload)
	req, err := http.NewRequest("POST", u, buf)
	c.setAuthorization(req)

	// send request
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// process response
	switch resp.StatusCode {
	case http.StatusOK:
		var respObj = Advisory{}
		if err := json.NewDecoder(resp.Body).Decode(&respObj); err != nil {
			return nil, err
		}
		adv.UUID = respObj.UUID
		return &respObj, nil

	case http.StatusBadRequest, http.StatusUnprocessableEntity:
		errs := []InputError{}
		if err := json.NewDecoder(resp.Body).Decode(&errs); err != nil {
			return nil, err
		}
		return nil, InputErrors(errs)

	default:
		return nil, ErrUnexpectedStatus{resp.StatusCode, resp.Status}
	}
}
