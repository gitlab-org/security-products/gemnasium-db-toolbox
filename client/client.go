package client

import (
	"net/http"
	"strings"

	"github.com/urfave/cli"
)

const (
	paramJWT       = "jwt"
	paramServerURL = "server-url"
)

// Flags returns new client flags
func Flags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   paramJWT,
			Usage:  "JSON Web Token",
			EnvVar: "GEMNASIUM_JWT",
		},
		cli.StringFlag{
			Name:   paramServerURL,
			Usage:  "Server URL",
			EnvVar: "GEMNASIUM_SERVER_URL",
			Value:  "https://deps.sec.gitlab.com/",
		},
	}
}

// NewClient returns a new client
func NewClient(c *cli.Context) *Client {
	return &Client{
		baseURL: strings.TrimSuffix(c.String(paramServerURL), "/"),
		jwt:     c.String(paramJWT),
	}
}

// Client contains a url and jwt
type Client struct {
	baseURL string
	jwt     string
}

func (c Client) setAuthorization(req *http.Request) {
	req.Header.Set("Authorization", "Bearer "+c.jwt)
}
