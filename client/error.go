package client

import (
	"fmt"
	"strings"
)

// ErrUnexpectedStatus defines a unexpected status error
type ErrUnexpectedStatus struct {
	statusCode int
	status     string
}

func (e ErrUnexpectedStatus) Error() string {
	return fmt.Sprintf("unexpected status: %d %s", e.statusCode, e.status)
}

// InputError is the type of structured validation errors returned by the Gemnasium API
type InputError struct {
	ErrorMsg string `json:"error"`
	Field    string `json:"field,omitempty"`
	Details  string `json:"details,omitempty"`
	Value    string `json:"value,omitempty"`
}

func (err InputError) Error() string {
	parts := []string{}
	if err.Field != "" {
		parts = append(parts, err.Field)
	}
	if err.Value != "" {
		parts = append(parts, err.Value)
	}
	if len(parts) > 0 {
		return strings.Join(parts, ", ") + ": " + err.ErrorMsg
	} 
	return err.ErrorMsg
}

// InputErrors is a list of input errors
type InputErrors []InputError

func (errs InputErrors) Error() string {
	parts := []string{}
	for _, err := range errs {
		parts = append(parts, err.Error())
	}
	return strings.Join(parts, "\n")
}
