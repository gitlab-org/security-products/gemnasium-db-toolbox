package client

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

// GetVersions retreives a list of versions  
func (c Client) GetVersions(pkgSlug string) ([]model.Version, error) {
	// prepare request
	var u = fmt.Sprintf("%s/api/packages/%s/versions", c.baseURL, pkgSlug)
	req, err := http.NewRequest("GET", u, nil)
	c.setAuthorization(req)

	// send request
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// process response
	switch resp.StatusCode {
	case http.StatusOK:
		var result = []model.Version{}
		if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, ErrUnexpectedStatus{resp.StatusCode, resp.Status}
	}
}
