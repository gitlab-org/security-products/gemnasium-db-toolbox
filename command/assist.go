package command

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/manifoldco/promptui"
	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/client"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
	"gopkg.in/guregu/null.v3"
	"gopkg.in/yaml.v2"
)

func askDate(defaultDate string, question string) (string, error) {
	// 4. Ask for publication date
	getPubdate := promptui.Prompt{
		Label: question,
		Validate: func(input string) error {
			_, err := time.Parse("2006-01-02", input)
			return err
		},
		Default: defaultDate,
	}
	return getPubdate.Run()
}

// Assist runs the advisory assistant
func Assist() cli.Command {
	return cli.Command{
		Name:  "assist",
		Usage: "Interactive assistant that helps with the advisory generation",
		Flags: client.Flags(),
		Action: func(c *cli.Context) error {

			var adv = model.Advisory{}

			// 1. Ask whether advisory is identifiable through NVD
			iscve := promptui.Prompt{
				Label:     "Is the advisory related to a CVE",
				IsConfirm: true,
			}

			_, err := iscve.Run()

			identifier := "tbd"

			if err != nil {
				adv.Identifier = null.StringFrom("to-be-defined")
			} else {
				cvePattern, err := regexp.Compile("CVE-\\d{4}-\\d{4,7}")
				if err != nil {
					return err
				}
				getIdentifier := promptui.Prompt{
					Label: "Enter identifier (e.g., CVE-2020-0001)",
					Validate: func(input string) error {
						if cvePattern.MatchString(input) {
							return nil
						}
						return errors.New("malformed CVE pattern")
					},
					Default: "",
				}
				identifier, err = getIdentifier.Run()
				if err != nil {
					return err
				}
				adv.Identifier = null.StringFrom(identifier)
			}

			// 2. Ask for package type
			selectType := promptui.Select{
				Label: "To which package type is the advisory related?",
				Items: []string{"go", "npm", "maven", "packagist", "pypi", "ruby"},
			}

			_, packageType, err := selectType.Run()

			// pattern may be a bit too open -- just want to avoid to be too restrictive here
			nonEmptyString, err := regexp.Compile(".+")
			if err != nil {
				return err
			}
			getPackageName := promptui.Prompt{
				Label: "Enter package name",
				Validate: func(input string) error {
					if nonEmptyString.MatchString(input) {
						return nil
					}
					return errors.New("malformed package name")
				},
				Default: "",
			}
			packageName, err := getPackageName.Run()
			if err != nil {
				return err
			}

			adv.PackageSlug = fmt.Sprintf("%s/%s/%s", packageType, packageName, identifier)

			// 2. Ask for title
			getTitle := promptui.Prompt{
				Label: "Short description of vulnerability, preferably a known CWE",
				Validate: func(input string) error {
					if nonEmptyString.MatchString(input) {
						return nil
					}
					return errors.New("malformed title")
				},
				Default: "",
			}

			title, err := getTitle.Run()
			if err != nil {
				return err
			}

			adv.Title = title

			// 3. Ask for description
			getDescription := promptui.Prompt{
				Label: "Long description of vulnerability, please omit version info",
				Validate: func(input string) error {
					if nonEmptyString.MatchString(input) {
						return nil
					}
					return errors.New("malformed description")
				},
				Default: "",
			}
			description, err := getDescription.Run()
			if err != nil {
				return err
			}

			adv.Description = description

			time := time.Now()
			stime := fmt.Sprintf("%d-%02d-%02d", time.Year(), time.Month(), time.Day())

			// 4. Ask for publication date
			adv.DisclosureDate, err = askDate(stime,
				"Date on which the vulnerability was made public")
			if err != nil {
				return err
			}

			// 5. Ask for change date
			adv.ModificationDate, err = askDate(adv.DisclosureDate,
				"Date on which the vulnerability has been changed")
			if err != nil {
				return err
			}

			defaultRange := ">=0"
			example := ">=1.4,<1.5||>=1.6,<1.7"
			if packageType == "maven" {
				defaultRange = "[0,)"
				example = "(,2.6.9),[3.0.0,3.0.1)"
			}

			// 6. Ask for affected range
			rangePattern, err := regexp.Compile("[<>= ,\\]\\[\\)\\(\\w\\.|]*")
			if err != nil {
				return err
			}
			getAffectedRange := promptui.Prompt{
				Label: fmt.Sprintf("Affected versions (e.g., %s)", example),
				Validate: func(input string) error {
					if rangePattern.MatchString(input) {
						return nil
					}
					return errors.New("malformed affected range")
				},
				Default: defaultRange,
			}
			affectedRange, err := getAffectedRange.Run()
			if err != nil {
				return err
			}
			adv.AffectedRange = affectedRange

			// 7. Ask for fixed version
			versionPattern, err := regexp.Compile("^([\\d\\.a-zA-Z_\\-,]+)$")
			example = "1.2, 3.5"
			if err != nil {
				return err
			}
			getFixedVersions := promptui.Prompt{
				Label: fmt.Sprintf("Comma-separated list of fixed versions (e.g., %s)", example),
				Validate: func(input string) error {
					if versionPattern.MatchString(input) {
						return nil
					}
					return errors.New("malformed affected range")
				},
				Default: "",
			}
			fixedVersions, err := getFixedVersions.Run()
			if err != nil {
				return err
			}

			adv.FixedVersions = strings.Split(fixedVersions, ",")

			// 8. Ask for credit field
			CreditPattern, err := regexp.Compile("^[\\w\\s]*$")
			if err != nil {
				return err
			}
			getCredit := promptui.Prompt{
				Label: "Credit to",
				Validate: func(input string) error {
					if CreditPattern.MatchString(input) {
						return nil
					}
					return errors.New("malformed credit; use only alphanumeric characters and spaces)")
				},
				Default: "",
			}

			credit, err := getCredit.Run()
			if err != nil {
				return err
			}

			adv.Credit = null.StringFrom(credit)

			// 9. Generate UUID (clash is unlikely but will be checked in the CI job anyway)
			adv.UUID = uuid.New().String()

			d, err := yaml.Marshal(&adv)
			fmt.Printf("%s\n", string(d))

			return nil
		},
	}
}
