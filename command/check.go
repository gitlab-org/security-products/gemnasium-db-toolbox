package command

import (
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/client"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

// Check creates a new check command
func Check() cli.Command {
	return cli.Command{
		Name:  "check",
		Usage: "Make the server check the advisory",
		Flags: client.Flags(),
		Action: func(c *cli.Context) error {
			// check arguments
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs{1}
			}
			var path = c.Args().First()

			// load advisory
			adv, err := model.LoadAdvisory(path)
			if err != nil {
				return err
			}

			// preview advisory; ignore result
			_, err = client.NewClient(c).PostAdvisory(adv, client.PostModePreview)
			return err
		},
	}
}
