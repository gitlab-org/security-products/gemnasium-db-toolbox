package command

import (
	"fmt"

	"github.com/urfave/cli"
	"gopkg.in/guregu/null.v3"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

// Create returns a create command
func Create() cli.Command {
	return cli.Command{
		Name:      "create",
		Usage:     "Create a new advisory file",
		ArgsUsage: "<package-slug> <advisory-id>",
		Action: func(c *cli.Context) error {
			// check arguments
			if len(c.Args()) != 2 {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs{2}
			}
			var pkgSlug = c.Args().Get(0)
			var advisoryID = c.Args().Get(1)

			// create advisory
			adv := &model.Advisory{Identifier: null.StringFrom(advisoryID), PackageSlug: pkgSlug}
			if err := model.WriteAdvisory(adv, adv.Path()); err != nil {
				return err
			}

			// show path
			fmt.Println(adv.Path())
			return nil
		},
	}
}
