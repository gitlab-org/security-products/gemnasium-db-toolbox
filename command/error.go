package command

import "fmt"

// ErrWrongNumberOfArgs is a wrong number of args error
type ErrWrongNumberOfArgs struct {
	expected int
}

func (e ErrWrongNumberOfArgs) Error() string {
	return fmt.Sprintf("wrong number of arguments, expected %d arg(s)", e.expected)
}
