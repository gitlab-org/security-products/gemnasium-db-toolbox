package command

import (
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/univers"
)

// FilterVersions creates a filter command
func FilterVersions() cli.Command {
	return cli.Command{
		Name:    "filter",
		Aliases: []string{"filter-versions"},
		Usage:   "Filter affected versions, keep those matching the affected range",
		Action: func(c *cli.Context) error {
			// check arguments
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs{1}
			}
			var path = c.Args().First()

			// load advisory
			adv, err := model.LoadAdvisory(path)
			if err != nil {
				return err
			}

			// get version syntax
			syntax, err := univers.NewSyntax(adv.Syntax())
			if err != nil {
				return err
			}

			// filter affected versions
			var input = adv.AffectedVersions
			var matchOpts = univers.MatchOptions{IncludePrerelease: true}
			output, err := syntax.Match(adv.AffectedRange, input, matchOpts)
			if err != nil {
				return err
			}
			adv.AffectedVersions = output

			// update file content
			return model.WriteAdvisory(adv, path)
		},
	}
}
