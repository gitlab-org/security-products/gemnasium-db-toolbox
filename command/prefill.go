package command

import (
	"sort"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/client"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

const paramSortVersions = "sort-versions"

// PrefillVersions creates a prefile command
func PrefillVersions() cli.Command {
	return cli.Command{
		Name:    "prefill",
		Aliases: []string{"prefill-versions"},
		Usage:   "Prefill an advisory, add package versions to affected list",
		Flags: append(client.Flags(),
			cli.BoolTFlag{
				Name:  paramSortVersions,
				Usage: "Sort versions",
			}),
		Action: func(c *cli.Context) error {
			// check arguments
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs{1}
			}
			var path = c.Args().First()

			// load advisory
			adv, err := model.LoadAdvisory(path)
			if err != nil {
				return err
			}

			// get version numbers
			versions, err := client.NewClient(c).GetVersions(adv.PackageSlug)
			if err != nil {
				return err
			}
			var vnums = versionNumbers(versions)

			// sort in increasing order
			if c.Bool(paramSortVersions) {
				sort.Strings(vnums)
			}

			// add all versions to the affected versions list
			adv.AffectedVersions = vnums

			// update file content
			return model.WriteAdvisory(adv, path)
		},
	}
}

// versionNumbers returns the unique version numbers.
func versionNumbers(versions []model.Version) []string {
	var vnums = []string{}
	var seen = make(map[string]bool)
	for _, v := range versions {
		var vnum = v.Number
		if _, found := seen[vnum]; found {
			continue
		}
		vnums = append(vnums, vnum)
		seen[vnum] = true
	}
	return vnums
}
