package command

import (
	"fmt"
	"strings"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/client"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

// PreviewVersions creates a new preview command
func PreviewVersions() cli.Command {
	return cli.Command{
		Name:    "preview",
		Aliases: []string{"preview-versions"},
		Usage:   "Preview affected, fixed and ignored versions",
		Flags:   client.Flags(),
		Action: func(c *cli.Context) error {
			// check arguments
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs{1}
			}
			var path = c.Args().First()

			// load advisory
			adv, err := model.LoadAdvisory(path)
			if err != nil {
				return err
			}

			// preview advisory
			resp, err := client.NewClient(c).PostAdvisory(adv, client.PostModePreview)
			if err != nil {
				return err
			}
			versions := resp.Versions

			// build lists of versions
			fixed := []string{}
			affected := []string{}
			ignored := []string{}
			for _, v := range versions {
				switch {
				case v.Fixed:
					fixed = append(fixed, v.Number)
				case v.Affected:
					affected = append(affected, v.Number)
				default:
					ignored = append(ignored, v.Number)
				}
			}

			// show versions
			var output = []struct {
				name     string
				versions []string
			}{
				{"fixed", fixed},
				{"affected", affected},
				{"ignored", ignored},
			}
			for _, o := range output {
				fmt.Println(o.name+":", strings.Join(o.versions, " "))
			}
			return nil
		},
	}
}
