package command

import (
	"fmt"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/client"
	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/model"
)

// Publish creates a new cli publish command
func Publish() cli.Command {
	return cli.Command{
		Name:  "publish",
		Usage: "Publish a new advisory",
		Flags: client.Flags(),
		Action: func(c *cli.Context) error {
			// check arguments
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return ErrWrongNumberOfArgs{1}
			}
			var path = c.Args().First()

			// load advisory
			adv, err := model.LoadAdvisory(path)
			if err != nil {
				return err
			}

			// publish advisory; UUID is set
			resp, err := client.NewClient(c).PostAdvisory(adv, client.PostModePublish)
			if err != nil {
				return err
			}

			// log UUID so that it can be set manually if file cannot be saved
			fmt.Fprintln(os.Stderr, "uuid:", resp.UUID)

			// clear affected versions list
			adv.AffectedVersions = nil

			// update file content
			return model.WriteAdvisory(adv, path)
		},
	}
}
