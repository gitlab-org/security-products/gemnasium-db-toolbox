module gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/manifoldco/promptui v0.6.0
	github.com/urfave/cli v1.20.0
	gopkg.in/guregu/null.v3 v3.4.0
	gopkg.in/yaml.v2 v2.2.2
)
