package main

import (
	"log"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/command"
)

func main() {
	app := cli.NewApp()
	app.Name = "gemnasium"
	app.Usage = "Client to publish security advisories to Gemnasium"
	app.Author = "GitLab"
	app.Email = "gl-secure@gitlab.com"

	app.Commands = []cli.Command{
		command.Create(),
		command.Check(),
		command.PreviewVersions(),
		command.PrefillVersions(),
		command.FilterVersions(),
		command.Publish(),
		command.Assist(),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
