package model

import (
	"path/filepath"
	"strings"

	"gopkg.in/guregu/null.v3"
)

// FileExtension is a .yml file extension
const FileExtension = ".yml"

// Advisory is a gemnasium advisory
type Advisory struct {
	Identifier       null.String `json:"identifier" yaml:"identifier"`
	Title            string      `json:"title" yaml:"title"`
	Description      string      `json:"description" yaml:"description"`
	ModificationDate string      `json:"date" yaml:"date"`
	DisclosureDate   string      `json:"pubdate" yaml:"pubdate"`
	AffectedRange    string      `json:"affected_range" yaml:"affected_range"`                                       // parsed
	AffectedVersions []string    `json:"affected_versions_array,omitempty" yaml:"affected_versions_array,omitempty"` // write-only
	FixedVersions    []string    `json:"fixed_versions" yaml:"fixed_versions"`                                       // parsed
	ImpactedVersions null.String `json:"affected_versions" yaml:"affected_versions"`                                 // human friendly
	NotImpacted      null.String `json:"not_impacted,omitempty" yaml:"not_impacted,omitempty"`                       // human friendly
	Solution         null.String `json:"solution" yaml:"solution"`
	Links            []string    `json:"urls" yaml:"urls"`
	Credit           null.String `json:"credit,omitempty" yaml:"credit,omitempty"`
	CvssV2           null.String `json:"cvss_v2" yaml:"cvss_v2"`
	CvssV3           null.String `json:"cvss_v3" yaml:"cvss_v3"`
	PackageSlug      string      `json:"package_slug" yaml:"package_slug"`
	UUID             string      `json:"uuid,omitempty" yaml:"uuid,omitempty"`
}

// Path returns an advisory path
func (a Advisory) Path() string {
	return filepath.Join(a.Dir(), a.Basename())
}

// Dir returns the package slug
func (a Advisory) Dir() string {
	return a.PackageSlug
}

// Basename returns the base filename
func (a Advisory) Basename() string {
	return a.Identifier.String + FileExtension
}

// Syntax returns the root package slug
func (a Advisory) Syntax() string {
	var parts = strings.Split(a.PackageSlug, "/")
	return parts[0]
}
