package model

import (
	"os"

	"gopkg.in/yaml.v2"
)

// LoadAdvisory creates a new Advisory
func LoadAdvisory(path string) (*Advisory, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	adv := Advisory{}
	err = yaml.NewDecoder(f).Decode(&adv)
	return &adv, err
}

// WriteAdvisory writes an Advisory to a file defined by the path argument 
func WriteAdvisory(adv *Advisory, path string) error {
	if err := os.MkdirAll(adv.Dir(), 0755); err != nil {
		return err
	}
	f, err := os.OpenFile(adv.Path(), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	if err = yaml.NewEncoder(f).Encode(adv); err != nil {
		return err
	}
	return nil
}
