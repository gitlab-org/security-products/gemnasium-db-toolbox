package model

// Version is a gemnasium version 
type Version struct {
	Number   string `json:"number"`
	Fixed    bool   `json:"fixed"`
	Affected bool   `json:"Affected"`
	// platform
	// published_at
}
