package npm

import (
	"os"
	"os/exec"
	"strings"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/univers"
)

const pathGemver = "gemver"

// Syntax satisfies a syntax gem interface
type Syntax struct {
}

// Match runs a gem command
func (s Syntax) Match(vrange string, versions []string, opts univers.MatchOptions) ([]string, error) {
	// run command
	// TODO: process match options
	var args = append([]string{vrange}, versions...)
	cmd := exec.Command(pathGemver, args...)
	cmd.Stderr = os.Stderr
	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	// split output
	return strings.Fields(string(output)), nil
}

func init() {
	univers.RegisterSyntax("gem", Syntax{})
}
