#!/usr/bin/ruby

# Filter a list of versions and print those matching the given requirement.
#
# $ gemver.rb "~>1.0.rc1 <2.0 || >=3.0" 1.0.rc1 1.0 1.2.rc2 1.2.0 1.3.0.rc3 2.0.rc2 2.0 3.0
# 1.0.rc1
# 1.0
# 1.2.rc2
# 1.2.0
# 1.3.0.rc3
# 3.0

def parse_requirements(reqs)
  reqs.strip.split(%r{\s*\|\|\s*}).map do |req|
    Gem::Requirement.new(req.split(%r{\s+}).map{|r| r.strip})
  end
end

def parse_versions(versions)
  versions.map{|v| Gem::Version.new(v)}
end

if ARGV.size < 2
  STDERR.puts <<-EOS
Filter versions, return versions matching requirement.
usage: gemver.rb <requirement> <version> ...
EOS
  exit 1
end

def main
  req_string = ARGV.shift
  reqs = parse_requirements(req_string)
  versions = parse_versions(ARGV)

  versions.select do |v|
    reqs.any?{|req| req === v}
  end.each do |v|
    puts v
  end

rescue Gem::Requirement::BadRequirementError
  STDERR.puts "cannot parse requirement: #{req_string}"
  exit 2
end

main
