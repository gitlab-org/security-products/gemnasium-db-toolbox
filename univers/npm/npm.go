package npm

import (
	"errors"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox/univers"
)

const pathSemver = "semver"

// ErrNotImplemented is not implemented error
var ErrNotImplemented = errors.New("not implemented")

// Syntax satisfies a syntax npm interface
type Syntax struct {
}

// Match runs a npm command
func (s Syntax) Match(vrange string, versions []string, opts univers.MatchOptions) ([]string, error) {

	// prepare arguments
	args := []string{"-r", vrange}
	if opts.IncludePrerelease {
		args = append(args, "--include-prerelease")
	}
	args = append(args, versions...)

	// run command
	cmd := exec.Command(pathSemver, args...)
	cmd.Stderr = os.Stderr
	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	// split output
	return strings.Fields(string(output)), nil
}

func init() {
	univers.RegisterSyntax("npm", Syntax{})
}
