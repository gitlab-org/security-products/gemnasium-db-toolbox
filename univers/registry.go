package univers

import (
	"sort"
	"sync"
)

var syntaxesMu sync.Mutex
var syntaxes = make(map[string]Syntax)

// RegisterSyntax registers a version syntax with a name.
func RegisterSyntax(name string, syntax Syntax) {
	syntaxesMu.Lock()
	defer syntaxesMu.Unlock()
	syntaxes[name] = syntax
}

// GetSyntax Retrieves a registered driver by API
func GetSyntax(api string) Syntax {
	syntaxesMu.Lock()
	defer syntaxesMu.Unlock()
	driver := syntaxes[api]
	return driver
}

// Syntaxes returns a sorted list of the syntaxes.
func Syntaxes() []string {
	syntaxesMu.Lock()
	defer syntaxesMu.Unlock()
	var list []string
	for syntax := range syntaxes {
		list = append(list, syntax)
	}
	sort.Strings(list)
	return list
}
