package univers

// Syntax is an interface used by both gem and npm univers
type Syntax interface {
	Match(vrange string, versions []string, opts MatchOptions) ([]string, error)
}

// MatchOptions is a prerelease option
type MatchOptions struct {
	IncludePrerelease bool
}

// NewSyntax creates a new syntax 
func NewSyntax(name string) (Syntax, error) {
	var syntax = GetSyntax(name)
	if syntax == nil {
		return nil, ErrSyntaxNotFound{name}
	}
	return syntax, nil
}

// ErrSyntaxNotFound is a syntax not found error
type ErrSyntaxNotFound struct {
	name string
}

func (e ErrSyntaxNotFound) Error() string {
	return "syntax not found: " + e.name
}
